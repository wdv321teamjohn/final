<?php

	if(isset($_POST['create_post'])){

		global $connection;

		$query = $connection->prepare("INSERT INTO posts(post_category_id, post_title, post_author, post_date, post_image, post_content, post_tag, post_comment_count, post_status) VALUES (?, ?, ?, ? ,?, ?,?,?,?)");
		
		$query->bind_param("issssssis",$post_category_id, $post_title, $post_author, $post_date, $post_image, $post_content, $post_tag, $post_comment_count, $post_status);

		$post_title = $_POST['title'];
		$post_author = $_POST['author'];
		$post_category_id = $_POST['post_category'];
		$post_status = $_POST['post_status'];

		$post_image = $_FILES['image']['name'];
		$post_image_temp = $_FILES['image']['tmp_name'];

		$post_tag = $_POST['post_tags'];
		$post_content = $_POST['post_content'];
		$post_date = date('Y-d-m');
		$post_comment_count = 4;

		move_uploaded_file($post_image_temp, "../images/$post_image");

		$create_post_query = $query->execute();
        
  		confirm($create_post_query);
	}

?>

<!--Form for adding to page-->
<form action="" method="post" enctype="multipart/form-data">
	
	<div class="form-group">
		<label for="title">Post Title</label>
		<input type="text" class="form-control" name="title">
	</div>

	<div class="form-group">
		<select name="post_category" id="">
			
		<?php
		//Create adding to form dropdown
			$query = "SELECT cat_id, cat_title FROM categories";
	        $select_categories = $connection->query($query);

	        confirm($select_categories);

	        while($row = $select_categories->fetch_assoc()){
	            $cat_id = $row['cat_id'];
	            $cat_title = $row['cat_title'];

	            echo "<option value='$cat_id'>{$cat_title}</option>";
	        }


		?>

		</select>
	</div>

	<div class="form-group">
		<label for="post_category">Post Category Id</label>
		<input type="text" class="form-control" name="post_category_id">
	</div>

	<div class="form-group">
		<label for="author">Post Author</label>
		<input type="text" class="form-control" name="author">
	</div>

	<div class="form-group">
		<label for="post_status">Post Status</label>
		<input type="text" class="form-control" name="post_status">
	</div>

	<div class="form-group">
		<label for="image">Post Image</label>
		<input type="file" name="image">
	</div>

	<div class="form-group">
		<label for="post_tags">Post Tags</label>
		<input type="text" class="form-control" name="post_tags">
	</div>

	<div class="form-group">
		<label for="post_content">Post Content</label>
		<textarea type="text" class="form-control" name="post_content" id="" cols="30" rows="10"></textarea>
	</div>

	<div class="form-group">
		<input class="btn btn-primary" type="submit" name="create_post" value="Submit">
	</div>
</form>