<?php

class email{

	public $from;
	public $to;
	public $subject;
	public $message;
	public $reason;
	public $mail;
	public $infoProduct;

	function set_to($newTo){
		$this->to = $newTo;
	}
	function get_to() {		
		return $this->to;		
	}

	function set_from($newFrom){
		$this->from = $newFrom;
	}
	function get_from() {		
		return $this->from;		
	}

	function set_subject($newSubject){
		$this->subject = $newSubject;
	}
	function get_subject() {		
		return $this->subject;		
	}

	function set_message($newMessage){
		$this->message = $newMessage;
	}
	function get_message() {		
		return $this->message;		
	}

	function set_reason($newReason){
		$this->reason = $newReason;
	}

	function get_reason(){
		return $this->reason;
	}
}

?>