<?php  include 'Email.php' ?>
<?php  include '../../includes/db.php' ?>
<?php

	// if(isset($_POST['submitBtn'])){
	// 	echo "working";
	// }

	$nameErr = "";
	$emailErr = "";
	$reasonErr = "";
	$messageErr = "";
	$sqlSuccess = "";
	$sqlErr = "";
	$dateSubmitted = date("m/d/Y");
	$timeSubmitted = gmdate("h:i");

	$validForm = false;

	$inName = "";
	$inEmail = "";
	$inReason = "";
	$inMessage = "";

	//***********Email Section***********
	$email = new email();

	  $email->set_to("jdiercks@johndiercks.biz");
	  $email->set_from($_POST['email']);
	  $email->set_subject($_POST['reasonSelect']);
	  $email->set_message($_POST['comments']);
	  $inMailingList = $_POST['mailingList'];
	  $inMoreInfo = $_POST['moreInfoAboutProducts'];

	function validateName(){

		global $inName, $validForm, $nameErr;

		$trimmedName = str_replace(' ', '', $inName);

	    $sanitizedString = filter_var($trimmedName, FILTER_SANITIZE_STRING);

	    $nameErr = "";

	    if($trimmedName == ""){

	      $nameErr = "Please enter a valid name";
	      $validForm = false;

	    }
	    else if(ctype_alpha($trimmedName) == false){

	      $nameErr = "Please only enter letters";
	      $validForm = false;
	    }
	}

	function valdiateEmail(){

		global $inEmail, $validForm, $emailErr;

		$emailErr = "";

		if(!filter_var($inEmail, FILTER_VALIDATE_EMAIL)){
			$emailErr = "Please enter a valid email";
			$validForm = false;
		}
	}

	function validateReason(){

		global $inReason, $validForm, $reasonErr, $inMessage, $messageErr;

		$reasonErr = "";
		$default = 'default';
		$other = 'other';
		$removeSpaces = str_replace(' ', '', $inMessage);

		if($inReason === $default){
			$reasonErr = "Please select a reason";
			$validForm = false;
		}
		else if($inReason === 'other'){
			if($removeSpaces == ""){
				$validForm = false;
				$messageErr = "Please enter something";
			}
		}
		else if(!$inReason){
			$validForm = false;
		}
	}

	//start of prepared statement
	//prepare and bind parameters
	function sqlStmt(){
		include '../../includes/db.php'; //connects to the database

		global $inReason, $validForm, $sqlErr, $inName, $inEmail, $inReason, $inMessage;

		

		//declare variables for dropdown
		$problem = 'problem';
		$return = 'return';
		$question = 'question';
		$website = 'website';
		$other = 'other';

		//start of the prepared statement and inserting into database
		$stmt = $connection->prepare("INSERT INTO wdv_341_customer_contacts (contact_name, contact_email, contact_reason, contact_comments, contact_newsletter, contact_more_products, contact_date, contact_time, contact_assigned_rep, followup_date, followup_result) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

		//binding variables
		$stmt->bind_param("sssssssssss", $contact_name, $contact_email, $contact_reason, $contact_comments, $contact_newsletter, $contact_more_products, $contact_date, $contact_time, $contact_assigned_rep, $followup_date, $followup_result);

		//set parameters and execute
		$contact_name = $_POST['enterName'];
		$contact_email = $_POST['email'];
		$contact_reason = $_POST['reasonSelect'];
		$contact_comments = $_POST['comments'];
		$contact_newsletter = $_POST['mailingList'];
		$contact_more_products = $_POST['moreInfoAboutProducts'];
		$contact_date = date("Y-m-d");
		$contact_time = gmdate("h:i");
		$contact_assigned_rep = "Jimmy Buffet";
		$followup_date = date("Y-m-d", strtotime('2016-11-30'));
		$followup_result = "amazing";

		if(empty($inName)){
			 $validForm = false;
		}
		else if(empty($inEmail)){
			 $validForm = false;
		}
		else if($inReason === 'default'){
			 $validForm = false;
		}
		else if($inReason === 'other'){

			if(empty($inMessage)){
			 	$validForm = false;
			}

		}
		else{

			if($stmt->execute()){
				$sqlSuccess = "Your are now in our system records created successfully.  ";
			}
			else{
				$message = "We are sorry ";
				echo $connection->error();
				$validForm = false;
				$sqlErr = "Please, enter your information again";
			}

		}

	}	



	if(isset($_POST['submitBtn'])){

		$validForm = true;

		$inName = $_POST['enterName'];
		$inEmail = $_POST['email'];
		$inReason = $_POST['reasonSelect'];
		$inMessage = $_POST['comments'];

		sqlStmt();
		$validation = validateName();
		$validation =valdiateEmail();
		$validation =validateReason();
	}
?>

<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Email</title>
<style>
	#comments{
		width: 400px;
	}
	
	#comments:focus{
		background-color: lightblue;
	}
	
	#confHeading{
		text-align:center;
	}

	#confMain{
		text-align: left;
	}

	div {
		border-radius: 5px;
		background-color: #f2f2f2;
		padding: 20px;
		text-align:center;
	}
	
	input[type=text] {
		 width: 250px;
		 padding: 12px 20px;
		 margin: 8px 0;
		 box-sizing: border-box;
		 border: 1px solid #555;
		 outline: none;
	}
		
	input[type=text]:focus {
		background-color: lightblue;
	}
	
	input[type=submit] {
		width: 50%;
		background-color: #4CAF50;
		color: white;
		padding: 14px 20px;
		margin: 8px 0;
		border: none;
		border-radius: 4px;
		cursor: pointer;
	}
	
	#reasonSelect{
		width: 40%;
		text-align:center;
	}
	
	select {
		width: 75%;
		padding: 16px 20px;
		border: none;
		border-radius: 4px;
		background-color: white;
	}
	
	#submitBtn{
		width: 30%;	
	}


</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script>

</script>
</head>

<body>
<?php

if ($validForm)     //If the form has been entered and validated a confirmation page is displayed in the VIEW area.
{
?>
<div id="confHeading">
  <h3>Thank You!</h3>
  <p>Thank you for taking the time to fill this out. <?php echo "$sqlSuccess"; ?> Someone will contact you shortly.</p>
</div>
<div id="confMain">
  <?php

  echo nl2br("Name: " . $inName . "\n");
  echo nl2br("Email: " . $inEmail . "\n");
  echo nl2br("Reason for Contact: " . $inReason . "\n");
  echo nl2br("Comments: " . $inMessage . "\n");
  echo nl2br($inMailingList . "\n");
  echo nl2br($inMoreInfo . "\n");

  $to = $email->get_to();
  $subject = $email->get_subject();
  $message = $inName . "\r\n";
  $message .= $email->get_message() . "\r\n";
  $message .= $inMailingList . "\r\n";
  $message .= $inMoreInfo . "\r\n";
  $from = $email->get_from();

  mail($to, $subject, $message, $from);

  echo "submitted on " . $dateSubmitted . " at " . $timeSubmitted;
  ?>

  <p><a href="contactFormProject.php">Back to Previous</a></p>
</div>
<?php
} //end the true branch of the form view area
else
{ 
?>
<div>
    <form name="contactForm" method="post" action="contactFormProject.php">
      <p>&nbsp;</p>
      <?php echo "$sqlErr"; ?>
      <p>
        <label>Your Name: </label>
        <input type="text" name="enterName" id="enterName"><?php echo "$nameErr"; ?>
      </p>
      <p>Your Email: 
        <input type="text" name="email" id="email"><?php echo "$emailErr"; ?>
      </p>
      <p>Reason for contact: 
        <label for="reason">
          <select name="reasonSelect" id="reasonSelect">
            <option value="default" name="default">Please Select a Reason</option>
            <option value="Product Problem" name="problem">Product Problem</option>
            <option value="Return Product" name="return">Return a Product</option>
            <option value="Billing Question" name="question">Billing Question</option>
            <option value="Report a Website Problem" name="website">Report a Website Problem</option>
            <option value="other" name="other" id="other">Other</option>
          </select>
        </label><?php echo "$reasonErr"; ?>
      </p>
      <p>
        <label  id="commentsSection">Comments:
          <textarea name="comments" id="comments" cols="45" rows="5"></textarea><?php echo "$messageErr"; ?>
        </label>
      </p>
      <p>
        <label>
          <input type="checkbox" name="mailingList" id="mailingList" value="Yes" checked>
          Please put me on your mailing list.</label>
      </p>
      <p>
        <label>
          <input type="checkbox" name="moreInfoAboutProducts" id="moreInfoAboutProducts" value="Yes" checked>
          Send me more information</label>
      about your products.  </p>
      <p>
        <input type="hidden" name="hiddenField" id="hiddenField" value="application-id:US447">
      </p>
      <p>
        <input type="submit" name="submitBtn" id="submitBtn" value="Submit">
      </p>
      <p>
        <input type="reset" name="resetBtn" id="resetBtn" value="Reset">
      </p>
    </form>
</div>
<p>&nbsp;</p>
 <?php
}
	
  ?>
</body>
</html>
