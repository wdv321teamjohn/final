<?php

function confirm($result){
	global $connection;
	if(!$result){
		die("Query Failed:" . $connection->error());
	}

}

function insert_categories(){
	
	//create global connection
	global $connection;

	//if submit is pressed
	if(isset($_POST['submit'])){
		//set cat_title to variable
		$cat_title = $_POST['cat_title'];

		//check to see if cat_title is empty string or just empty in general and if it is display message
		if($cat_title == "" || empty($cat_title)){

		    echo "This field should not be empty";

		}

		//if not create statement and execute
		else
		{
			//insert prepare statement
			$stmt = $connection->prepare("INSERT INTO categories(cat_title) VALUES(?)");
			$stmt->bind_param("s", $cat_title);

		    $create_category_query = $stmt->execute();

		    if(!$create_category_query){
		        die('QUERY FAILED' . $connection->error());
		    }

		    $stmt->close();
		}

	}


}


function findAllCategories(){

	global $connection;

	//find all categories


//*****************************************create prepared statements**********************************************************	

	$query = "SELECT cat_id, cat_title FROM categories";
	$select_categories = $connection->query($query);

	while($row = mysqli_fetch_assoc($select_categories)){

	    $cat_id = $row['cat_id'];
	    $cat_title = $row['cat_title'];

	    echo "<tr>";

	    echo "<td>{$cat_id}</td>";
	    echo "<td>{$cat_title}</td>";

	    echo "<td><a href='categories.php?delete={$cat_id}'>Delete</a></td>";
	    echo "<td><a href='categories.php?edit={$cat_id}'>Update</a></td>";
	    echo "</tr>";
	}

}



function delete_categories(){

	//global connection
	global $connection;


	//delete categories
	if(isset($_GET['delete'])){

		//set get to variable
	    $the_cat_id = $_GET['delete'];

	    $query = "DELETE FROM categories WHERE cat_id = ?";
	    $stmt = $connection->prepare($query);	//prepare the statement

		$stmt->bind_param('i' ,$the_cat_id);	//bind the parameter to the statement

		if ( $stmt->execute() )			//process the query
		{
			$query = "DELETE FROM categories WHERE cat_id = $the_cat_id";
		}
		else
		{
			$message = "<h1>There has been a problem with the delete.</h1>";

			$message .= "<h2 style='color:red'>" . mysqli_error($connection) . "</h2>";
		}
	    // $delete_query = mysqli_query($connection, $query);
	    //used to refresh page
	    header("Location: categories.php");

	}
}
?>